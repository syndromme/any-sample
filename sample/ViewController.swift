//
//  ViewController.swift
//  sample
//
//  Created by syndromme on 28/07/20.
//  Copyright © 2020 syndromme. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var noDataContainerView: UIView!
  @IBOutlet weak var tableContainerView: UIView!
  @IBOutlet weak var tableView: UITableView!
  
  var users: [User] = []
  
  let services = Services.sharedInstance
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.tableFooterView = UIView()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    getAllUser()
  }
  
  @IBAction func addAction(_ sender: Any) {
    routeToForm(user: nil)
  }
  
  func getAllUser() {
    services.getAllUser(model: User.self, parameter: nil, sort: nil) { (results) in
      users = results as! [User]
      noDataContainerView.isHidden = users.count != 0
      tableContainerView.isHidden = users.count == 0
      tableView.reloadData()
    }
  }
  
  func removeUser(user: User) {
    services.deleteUser(model: User.self, parameter: NSPredicate(format: "userID = %@", user.userID!)) { (msg) in
      self.showAlert(message: msg)
      self.getAllUser()
    }
  }
  
  func showAlert(message: String) {
    let alert = UIAlertController(title: "ALERT", message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  
  func routeToForm(user: User?) {
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "UserFormVC") as! UserFormVC
    vc.users = users
    vc.currentUser = user
    vc.purpose = user == nil ? .NEW : .EDIT
    navigationController?.show(vc, sender: nil)
  }
}

extension ViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return users.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "akumaCell", for: indexPath)
    let user = users[indexPath.row]
    let nameLabel = cell.viewWithTag(1) as! UILabel
    let emailLabel = cell.viewWithTag(2) as! UILabel
    let phoneLabel = cell.viewWithTag(3) as! UILabel
    nameLabel.text = user.name
    emailLabel.text = user.email
    phoneLabel.text = user.phone
    cell.selectionStyle = .none
    return cell
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let editAction = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
      let user = self.users[indexPath.row]
      self.routeToForm(user: user)
    }
    editAction.backgroundColor = .orange
    
    let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
      let user = self.users[indexPath.row]
      self.removeUser(user: user)
    }
    deleteAction.backgroundColor = .red
    return [deleteAction, editAction]
  }
  
}

extension ViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
}
