//
//  Services.swift
//  sample
//
//  Created by syndromme on 28/07/20.
//  Copyright © 2020 syndromme. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Services: NSObject {
  
  fileprivate var appDelegate: AppDelegate
  fileprivate var mainContextInstance: NSManagedObjectContext

  class var sharedInstance : Services {
      struct Singleton {
          static let instance = Services.init()
      }
      return Singleton.instance
  }

  override init() {
      self.appDelegate = UIApplication.shared.delegate as! AppDelegate
      self.mainContextInstance = self.appDelegate.persistentContainer.viewContext
      super.init()
  }
  
  func getAllUser(model: AnyClass, distinct: Bool = false, distinctBy: [String] = [], parameter: NSPredicate?, sort:  [NSSortDescriptor]?, completion:(_ objects: [Any]) -> Void) {
    let entityName = NSStringFromClass(model)
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
    if distinct {
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.propertiesToFetch = [distinctBy]
        fetchRequest.returnsDistinctResults = distinct
    }
    if parameter != nil {
        fetchRequest.predicate = parameter
    }
    if sort != nil {
        fetchRequest.sortDescriptors = sort
    }
    var objects = [Any]()
    do {
        objects = try self.mainContextInstance.fetch(fetchRequest)
    } catch {
        completion([])
        return
    }
    completion(objects)
  }
  
  func createUser(model: AnyClass) -> NSManagedObject {
    let entityName = NSStringFromClass(model)
    let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.mainContextInstance)
    let context = NSManagedObject(entity: entity!, insertInto: self.mainContextInstance)
    return context
  }
  
  func saveUser(completion:@escaping (_ message: String) -> Void) {
    DispatchQueue.main.async {
      do {
        try self.mainContextInstance.save()
      } catch let error{
        completion(error.localizedDescription)
        return
      }
      completion("Success")
    }
  }
  
  func deleteUser(model: AnyClass, parameter: NSPredicate?, completion:@escaping (_ message: String) -> Void) {
    getAllUser(model: model, parameter: parameter, sort: nil) { (result) in
      if result.count > 0 {
        for object in result {
          self.mainContextInstance.delete(object as! NSManagedObject)
        }
        
        do {
          try self.mainContextInstance.save()
          completion("success")
        } catch {
          completion("error delete")
        }
      } else if result.count == 0 {
        completion("empty")
      } else {
        completion("error")
      }
    }
  }
}
