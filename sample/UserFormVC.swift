//
//  UserFormVC.swift
//  sample
//
//  Created by syndromme on 29/07/20.
//  Copyright © 2020 syndromme. All rights reserved.
//

import UIKit
import MapKit

class UserFormVC: UIViewController {
  @IBOutlet weak var nameField: UITextField!
  @IBOutlet weak var nameErrorLabel: UILabel!
  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var emailErrorLabel: UILabel!
  @IBOutlet weak var phoneField: UITextField!
  @IBOutlet weak var phoneErrorLabel: UILabel!
  @IBOutlet weak var addressField: UITextField!
  @IBOutlet weak var addressErrorLabel: UILabel!
  @IBOutlet weak var latitudeLabel: UILabel!
  @IBOutlet weak var longitudeLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var actionButton: UIButton!
  
  var purpose: Purpose = .NEW
  var users: [User] = []
  var currentUser: User?
  
  let services = Services.sharedInstance
  var center: CLLocationCoordinate2D?
  
  enum Purpose {
    case NEW
    case EDIT
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "User Form"
    actionButton.setTitle(purpose == .NEW ? "SAVE" : "UPDATE", for: .normal)
    if purpose == .NEW {
      center = ((UIApplication.shared.delegate as? AppDelegate)?.currentLocation)!
    } else {
      setCurrentUser()
    }
    setupMap()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  @IBAction func saveAction(_ sender: Any) {
    if purpose == .NEW {
      createNewUser()
    } else {
      updateUser()
    }
  }
  
  func setCurrentUser() {
    nameField.text = currentUser?.name
    emailField.text = currentUser?.email
    phoneField.text = currentUser?.phone
    addressField.text = currentUser?.address
    latitudeLabel.text = "\(currentUser?.latitude ?? 0)"
    longitudeLabel.text = "\(currentUser?.longitude ?? 0)"
    
    center = CLLocationCoordinate2D(latitude: CLLocationDegrees(currentUser?.latitude ?? 0), longitude: CLLocationDegrees(currentUser?.longitude ?? 0))
  }
  
  func createNewUser() {
    if validateInput() {
      let userContext = services.createUser(model: User.self) as! User
      userContext.userID = "User-\(users.count+1)"
      userContext.name = nameField.text
      userContext.email = emailField.text
      userContext.phone = phoneField.text
      userContext.address = addressField.text
      userContext.latitude = Float(center?.latitude ?? 0)
      userContext.longitude = Float(center?.longitude ?? 0)
      services.saveUser { (msg) in
        self.showAlert(message: msg)
      }
    }
  }
  
  func updateUser() {
    if validateInput() {
      let user = users.first
      user?.name = nameField.text
      user?.email = emailField.text
      user?.phone = phoneField.text
      user?.address = addressField.text
      user?.latitude = Float(center?.latitude ?? 0)
      user?.longitude = Float(center?.longitude ?? 0)
      services.saveUser { (msg) in
        self.showAlert(message: msg)
      }
    }
  }
  
  func showAlert(message: String) {
    let alert = UIAlertController(title: "ALERT", message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  
  func setupMap() {
    mapView.showsUserLocation = true
    let viewRegion = MKCoordinateRegion(center: center!, latitudinalMeters: 200, longitudinalMeters: 200)
    mapView.setRegion(viewRegion, animated: false)
    mapView.delegate = self
  }
  
  func validateInput() -> Bool {
    nameErrorLabel.text = ""
    emailErrorLabel.text = ""
    phoneErrorLabel.text = ""
    addressErrorLabel.text = ""
    if nameField.text?.isEmpty ?? true {
      nameErrorLabel.text = "Name is required"
      return false
    }
    if emailField.text?.isEmpty ?? true {
      emailErrorLabel.text = "Email is required"
      return false
    }
    if phoneField.text?.isEmpty ?? true {
      phoneErrorLabel.text = "Phone is required"
      return false
    }
    if addressField.text?.isEmpty ?? true {
      addressErrorLabel.text = "Address is required"
      return false
    }
    return true
  }
}
extension UserFormVC:UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
  }
}
extension UserFormVC: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
    center = mapView.centerCoordinate
    latitudeLabel.text = "\(center?.latitude ?? 0)"
    longitudeLabel.text = "\(center?.longitude ?? 0)"
  }
}
